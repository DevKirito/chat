package me.kiritodev.Test;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ChatServerSocket {
	
	public ServerSocket server;
	public List<ChatClient> users;
	private int id = 0;
	
	public ChatServerSocket(int port) throws IOException {
		this.server = new ServerSocket(port);
		this.users = new ArrayList<>();		
		System.out.println("Starting Server In Port "+port);
		new Thread(new Runnable() {
			@Override
			public void run() {
				synchronized(this){
					while(true){
						try {
							Socket s = server.accept();
							DataInputStream in = new DataInputStream(s.getInputStream());
							PrintStream os = new PrintStream(s.getOutputStream());
							id++;							
							String md = in.readLine();
							if(md.startsWith("!#")){
								String nick = md.substring(2).trim();
								ChatClient c = new ChatClient(s,nick,id);
								users.add(c);
								os.println("Welcome "+nick+" To PixelClient Chat Your ID Is: "+id);
							}						
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}			
		}).start();		
		new Thread(new Runnable() {
			@Override
			public void run() {
				synchronized(this) {
					while(true){
						for(ChatClient c : users) {							
							try {
								DataInputStream in = new DataInputStream(c.getSocket().getInputStream());
								PrintStream os = new PrintStream(c.getSocket().getOutputStream());
								String msg = in.readLine();
								if(msg != null) {
									os.println(c.getName()+": "+msg);
								}
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}	
				}
			}			
		}).start();	
	}	
	
	public class ChatClient{
		
		private Socket s;
		private String name;
		private int id;
		private PrintStream os;
		
		public ChatClient(Socket s, String name, int id) throws IOException{
			this.s = s;
			this.name = name;
			this.id = id;
			os = new PrintStream(this.s.getOutputStream());
		}
		
		public int getID() {
			return this.id;		
		}
		
		public void sendMessage(String msg){
			this.os.print(msg);
		}
		
		public Socket getSocket() {
			return this.s;
		}
		
		public String getName() {
			return this.name;
		}
	}
	
	public static void main(String[] args) throws IOException{
		new ChatServerSocket(10000);
	}
}
