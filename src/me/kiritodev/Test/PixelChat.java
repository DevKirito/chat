package me.kiritodev.Test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class PixelChat {
	
	private BufferedWriter bwriter;
	
	static void sendString(BufferedWriter bw, String str) {
	    try {
	      bw.write(str + "\r\n");
	      bw.flush();
	    }
	    catch (Exception e) {
	      System.out.println("Exception: "+e);
	    }
	  }
	  public void start(String args[]) {
	    try {

	      String server   = "chat1.ustream.tv";
	      int port        = 6667;
	      String nickname = "ustreamer-183999bot";
	      String channel  = "#bot-test-ch";
	      String message  = "hi, all";

	      Socket socket = new Socket(server,port);
	      OutputStreamWriter outputStreamWriter = new OutputStreamWriter(socket.getOutputStream());
	      bwriter = new BufferedWriter(outputStreamWriter);

	      sendString(bwriter,"NICK "+nickname);
	      sendString(bwriter,"USER chatterBot  8 * :chatterBot 0.0.1 Java IRC Bot - www.chat.org");
	      sendString(bwriter,"JOIN "+channel);

	      InputStreamReader inputStreamReader = new InputStreamReader(socket.getInputStream());
	      BufferedReader breader = new BufferedReader(inputStreamReader);
	      String line = null;
	      int tries = 1;
	      while ((line = breader.readLine()) != null) {
	        System.out.println(">>> "+line);
	        int firstSpace = line.indexOf(" ");
	        int secondSpace = line.indexOf(" ", firstSpace + 1);
	        if (secondSpace >= 0) {
	          String code = line.substring(firstSpace+1, secondSpace);
	          if (code.equals("004")) {
	            break;
	          }
	        }
	      }
	      
	      sendString(bwriter,"PRIVMSG "+channel+" :"+message);
	      
	      bwriter.close();
	    }catch (Exception e) {
	      e.printStackTrace();
	    }
	  }
}