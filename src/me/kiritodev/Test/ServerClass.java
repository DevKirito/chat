package me.kiritodev.Test;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerClass{
	
	private ServerSocket server;
	private Socket[] users;
	public static int id = 0;
		
	public static void main(String[] args) throws IOException {					
		new ServerClass();
	}
	
	public ServerClass() {
		new Thread(new Runnable() {
			@Override
			public void run() {			
				try {
					int port = 10000;		
					server = new ServerSocket(port);
					users = new Socket[10000];
				} catch (IOException e1) {
					e1.printStackTrace();
				}		
				while(true) {
					try {
						id++;
						users[id] = server.accept();
						System.out.println("New User Logged ID: "+id);				
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}	
		}).start();
		
		new Thread(new Runnable() {
			@Override
			public void run() {			
				while(true) {
					for(int i = 0; i < id; i++) {
						Socket s = users[i];
					    try {
							DataInputStream is = new DataInputStream(s.getInputStream());
							PrintStream os = new PrintStream(s.getOutputStream());
							os.println(is.readLine());
							System.out.println(is.readLine()+" "+is.readUTF());
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}	
		}).start();
	}
		
}
